import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

//importação de todas as páginas

import page1 from '../pages/page1';
import page2 from '../pages/page2';
import page3 from '../pages/page3';
import page4 from '../pages/page4';
import page5 from '../pages/page5';
import page6 from '../pages/page6';
import page7 from '../pages/page7';
import page8 from '../pages/page8';
import page9 from '../pages/page9';
import page10 from '../pages/page10';
import page11 from '../pages/page11';
import page12 from '../pages/page12';

const Routes = createAppContainer(
    createStackNavigator({
        page1: {
            screen: page1,
            navigationOptions: {
                header: null
            }
        },
        page2: {
            screen: page2,
            navigationOptions: {
                header: null
            }
        },
        page3: {
            screen: page3,
            navigationOptions: {
                header: null
            }
        },
        page4: {
            screen: page4,
            navigationOptions: {
                header: null
            }
        },
        page5: {
            screen: page5,
            navigationOptions: {
                header: null
            }
        },
        page6: {
            screen: page6,
            navigationOptions: {
                header: null
            }
        },
        page7: {
            screen: page7,
            navigationOptions: {
                header: null
            }
        },
        page8: {
            screen: page8,
            navigationOptions: {
                header: null
            }
        },
        page9: {
            screen: page9,
            navigationOptions: {
                header: null
            }
        },
        page10: {
            screen: page10,
            navigationOptions: {
                header: null
            }
        },
        page11: {
            screen: page11,
            navigationOptions: {
                header: null
            }
        },
        page12: {
            screen: page12,
            navigationOptions: {
                header: null
            }
        }
    }, 
    {
        initialRouteName: 'page1'
    })
);

export default Routes;
