import React, { Component } from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

import { CheckBox } from 'react-native-elements';

export default class Page10 extends Component {
  _onPressButton(){
    alert('confirmado!')
  }
  state = {
    one:false,
    two:false,
    three:false,
    four:false,
    five:false,
    six:false,
    seven:false,
    eight:false
  }
  onePressed(){
    this.setState({one:!this.state.one});
  } 
  twoPressed(){
   this.setState({two:!this.state.two});
  } 
  threePressed(){
   this.setState({three:!this.state.three});
  } 
  fourPressed(){
   this.setState({four:!this.state.four});
  } 
  fivePressed(){
   this.setState({five:!this.state.five});
  } 
  sixPressed(){
   this.setState({six:!this.state.six});
  } 
  sevenPressed(){
   this.setState({seven:!this.state.seven});
  } 
  eightPressed(){
   this.setState({eight:!this.state.eight});
  } 

  render() {
    return (
      <LinearGradient colors={['#F27281', '#345D7E']} style={styles.container}>
        <View>
          <View style={styles.logoContainer}>
            <TouchableOpacity style={{elevation: 3}} 
            onPress={()=> this.props.navigation.navigate('page1')}>
              <Image style={styles.buttonVoltar} source={require('../assets/images/voltar.png')} />
            </TouchableOpacity>
            <Image style={styles.logo} source={require('../assets/images/logo2.png')} />
          </View> 
          <Text style={styles.baseText}>OLÁ: MARIA DO CARMO</Text>
          <Text style={styles.baseText}>VOCÊ ESTÁ NO LOCAL: ÁREA EXTERNA</Text>
          <Text style={styles.baseText}>DO CLIENTE: SPSP</Text>
          <View style={styles.caixote}>
            <ScrollView>
              <Text style={styles.textinho}>
                INFORME O QUE FOI REALIZADO
              </Text> 
              <CheckBox
                title='LIMPAR E ESVAZIAR COLETORES'
                checked={this.state.one}
                containerStyle={styles.containerCheckbox}
                textStyle={styles.textCheckbox}
                uncheckedColor='#345D7E'
                checkedColor='#57BC90'
                onPress={()=>{this.onePressed()}}           
              />
              <CheckBox
                title='LIMPAR GELADEIRA'
                checked={this.state.two}
                containerStyle={styles.containerCheckbox}
                textStyle={styles.textCheckbox}
                uncheckedColor='#345D7E'
                checkedColor='#57BC90'
                onPress={()=>{this.twoPressed()}}             
              />
              <CheckBox
                title='LIMPAR ARMÁRIO'
                checked={this.state.three}
                containerStyle={styles.containerCheckbox}
                textStyle={styles.textCheckbox}
                uncheckedColor='#345D7E'
                checkedColor='#57BC90'
                onPress={()=>{this.threePressed()}}             
              />
              <CheckBox
                title='LIMPAR CAFETEIRA'
                checked={this.state.four}
                containerStyle={styles.containerCheckbox}
                textStyle={styles.textCheckbox}
                uncheckedColor='#345D7E'
                checkedColor='#57BC90'
                onPress={()=>{this.fourPressed()}}             
              />
              <CheckBox
                title='LIMPAR PIA E BALCÃO'
                checked={this.state.five}
                containerStyle={styles.containerCheckbox}
                textStyle={styles.textCheckbox}
                uncheckedColor='#345D7E'
                checkedColor='#57BC90'
                onPress={()=>{this.fivePressed()}}             
              />
              <CheckBox
                title='LIMPAR PISO'
                checked={this.state.six}
                containerStyle={styles.containerCheckbox}
                textStyle={styles.textCheckbox}
                uncheckedColor='#345D7E'
                checkedColor='#57BC90'
                onPress={()=>{this.sixPressed()}}             
              />
              <CheckBox
                title='LIMPAR PAREDES'
                checked={this.state.seven}
                containerStyle={styles.containerCheckbox}
                textStyle={styles.textCheckbox}
                uncheckedColor='#345D7E'
                checkedColor='#57BC90'
                onPress={()=>{this.sevenPressed()}}             
              />
              <CheckBox
                title='LIMPAR INTERRUPTORES'
                checked={this.state.eight}
                containerStyle={styles.containerCheckbox}
                textStyle={styles.textCheckbox}
                uncheckedColor='#345D7E'
                checkedColor='#57BC90'
                onPress={()=>{this.eightPressed()}}             
              />
              <TextInput style={styles.observacoes}
              placeholder='OBSERVAÇÕES'
              />
              <TouchableOpacity style={styles.buttonProx}
                onPress={()=>this.props.navigation.navigate('page11')}>
                <Text style={styles.textoLeitura}>CONFIRMAR</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>       
        </View>
      </LinearGradient>  
    );
  }
}

const styles = StyleSheet.create({  
  container: {
    flex: 1,
    flexDirection: 'column',
    height: '100%',
    width: '100%',
  },
  buttonVoltar: {
    marginVertical: '15%',
    marginLeft: '20%',
    width: 45,
    height: 40,
    resizeMode: 'contain',
  },
  logoContainer: {
    flexDirection: 'row',
    height: '12%',
    width: '100%',
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
  logo: {
    marginVertical: '5%',
    marginRight: '5%',
    width: 150,
    height: 50,
    resizeMode: 'contain',
  },
  baseText: {
    fontFamily: 'Abel Regular',
    fontSize: 15,
    flexDirection: 'row',
    marginLeft: '5%',
    marginTop: '3.5%'
  },
  caixote: {
    backgroundColor: '#C4C4C4',
    flexDirection: 'column',
    marginHorizontal: '10%',
    marginTop: '8%',
    marginBottom: '5%',
    width: '80%',
    height: '60%',
  },
  textinho: {
    marginHorizontal: 20,
    marginTop: 20,
    fontFamily: 'Abel Regular',
    fontSize: 20,
    marginBottom: 20,
  },
  buttonProx: {
    alignItems: 'center',
    backgroundColor: '#57BC90',
    height: 50,
    fontSize: 100,
    marginHorizontal: 50,
    marginBottom: 40,
    elevation: 3,
  },
  textoLeitura: {
    color: 'white',
    marginTop: 11,
    fontFamily: 'Abel Regular',
    fontSize: 18
  },
  containerCheckbox: {
    marginHorizontal: 10,
    marginVertical: 3,
    backgroundColor: '#345D7E',
    alignItems: 'center',
    elevation: 3,
  },
  textCheckbox: {
    color: 'white',
    fontFamily: 'Abel Regular',
  },
  observacoes: {
    color: 'black',
    backgroundColor: 'white',
    textAlign: 'center',
    marginHorizontal: 13,
    marginVertical: 20,
    elevation: 3,
    fontSize: 15,
    fontFamily: 'Abel Regular',
  },
});
