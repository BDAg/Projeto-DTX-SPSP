import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Button,
  Image,
  TouchableOpacity,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

export default class Page1 extends Component {
  render() {
    return (
        <LinearGradient colors={['#F27281', '#345D7E']} style={styles.container}>
          <View>
            <View style={styles.logoContainer}>
              <Image style={styles.logotafeito} source={require('../assets/images/logotafeito.jpeg')} />
              <Image style={styles.logospsp} source={require('../assets/images/logospsp.jpeg')} />
            </View>           
            <TouchableOpacity style={styles.buttonProx}
            onPress={()=>this.props.navigation.navigate('page2')}>
                <Text style={styles.textoLeitura}>LEITURA DO QRCODE</Text>
            </TouchableOpacity>
            <Text style={styles.textinho}>
              IDENTIFIQUE O LOCAL QUE VOCÊ ESTÁ FAZENDO A LEITURA DO QRCODE IDENTIFICADO COM A MARCA DO APP TA FEITO!
            </Text>
          </View>
      </LinearGradient>  
    );
  }
}

const styles = StyleSheet.create({  
  container: {
    flex: 1,
    flexDirection: 'column',
    height: '100%',
    width: '100%',
  },
  logoContainer: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    height: '35%',
    width: '100%',
    backgroundColor: 'white',
    elevation: 3,
  },
  logotafeito: {
    paddingTop: '1%',
    marginRight: '6%',
    width: '50%',
    height: '30%',
    resizeMode: 'contain',
  },
  logospsp: {
    marginTop: '1%',
    marginRight: '5%',
    width: '90%',
    height: '60%',
    resizeMode: 'contain',
  },
  buttonProx: {
    alignItems: 'center',
    backgroundColor: '#57BC90',
    height: '10%', 
    fontSize: 100,
    marginHorizontal: '10%',
    marginTop: '10%',
    elevation: 3,
  },
  textoLeitura: {
    color: 'white',
    paddingVertical: '5%',
    fontFamily: 'Abel Regular',
    fontSize: 25
  },
  textinho: {
    paddingHorizontal: '10%',
    textAlign: 'center',
    paddingVertical: '25%',
    fontFamily: 'Abel Regular',
    fontSize: 20,
  },
});
