import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  Image,
  TouchableOpacity,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

export default class Page9 extends Component {
  render() {
    return (
      <LinearGradient colors={['#F27281', '#345D7E']} style={styles.container}>
        <View>
          <View style={styles.logoContainer}>
            <TouchableOpacity style={{elevation: 3}} 
            onPress={()=> this.props.navigation.navigate('page8')}>
              <Image style={styles.buttonVoltar} source={require('../assets/images/voltar.png')} />
            </TouchableOpacity>
            <Image style={styles.logo} source={require('../assets/images/logo2.png')} />
          </View> 
          <Text style={styles.baseText}>OLÁ: MARIA DO CARMO</Text>
          <Text style={styles.baseText}>VOCÊ ESTÁ NO LOCAL: ÁREA EXTERNA</Text>
          <Text style={styles.baseText}>DO CLIENTE: SPSP</Text>
          <View style={styles.caixote}>
            <ScrollView>
                <Text style={styles.textinho}>
                TEMPO PERCORRIDO 00:13:25          
                </Text> 
                <Text style={styles.textinho}>
                VOCÊ DESEJA FINALIZAR A ROTINA SELECIONADA?          
                </Text> 
                <Text style={styles.textinho}>
                LIMPEZA PROGRAMADA 09:00 - 09:13         
                </Text> 
                <TouchableOpacity style={styles.buttonProx}
                onPress={()=>this.props.navigation.navigate('page10')}>
                <Text style={styles.textoLeitura}>FINALIZAR</Text>
                </TouchableOpacity>
            </ScrollView>
          </View>
          <View style={styles.caixote}>
            <ScrollView>
              <Text style={styles.textinho}>
                LIMPEZA PROGRAMADA 09:00 - 09:20                
              </Text> 
              <View style={styles.linha1}/> 
              <Text style={styles.textinho}>
                DESCRITIVO DA ROTINA            
              </Text> 
              <Text style={styles.programadas}>
                01 - LIMPAR E ESVAZIAR COLETORES
              </Text> 
              <Text style={styles.programadas}>
                02 - LIMPAR GELADEIRA
              </Text> 
              <Text style={styles.programadas}>
                03 - LIMPAR ARMÁRIO
              </Text> 
              <Text style={styles.programadas}>
                04 - LIMPAR CAFETEIRA
              </Text> 
              <Text style={styles.programadas}>
                05 - LIMPAR PIA E BALCÃO
              </Text> 
              <Text style={styles.programadas}>
                06 - LIMPAR PISO
              </Text> 
              <Text style={styles.programadas}>
                07 - LIMPAR PAREDES
              </Text> 
              <Text style={styles.programadas}>
                08 - LIMPAR INTERRUPTORES
              </Text>   
              <View style={styles.linha1}/> 
              <Text style={styles.textinho}>
                EPI E EPC           
              </Text> 
              <Text style={styles.programadas}>
                - UNIFORME
              </Text>   
              <Text style={styles.programadas}>
                - SAPATO DE SEGURANÇA
              </Text>  
              <Text style={styles.programadas}>
                - BOTA DE PVC
              </Text>  
              <Text style={styles.programadas}>
                - LUVA DE LÁTEX
              </Text>  
              <View style={styles.linha1}/> 
              <Text style={styles.textinho}>
                MÁQUINAS E EQUIPAMENTOS           
              </Text> 
              <Text style={styles.programadas}>
                - PANO
              </Text>
              <Text style={styles.programadas}>
                - ESPONJA DUPLA FACE
              </Text>
              <Text style={styles.programadas}>
                - MOP BIO
              </Text>
              <Text style={styles.programadas}>
                - PINCEL
              </Text>
              <Text style={styles.programadas}>
                - SACO PLÁSTICO
              </Text>
              <View style={styles.linha1}/> 
              <Text style={styles.textinho}>
                PRODUTOS          
              </Text> 
              <Text style={styles.programadas}>
                - DETERGENTE
              </Text>
              <Text style={styles.programadas}>
                - MULTIUSO
              </Text>
              <Text style={styles.programadas}>
                - ÁLCOOL
              </Text>
              <Text style={styles.programadas}>
                - DESINFETANTE
              </Text>
              <Text style={styles.programadas}>
                - ÁGUA SANITÁRIA
              </Text>
            </ScrollView>
          </View>       
        </View>
      </LinearGradient>  
    );
  }
}

const styles = StyleSheet.create({  
  linha1: {
    width: 280, 
    height: 2, 
    flex:1, 
    backgroundColor: 'white', 
    marginTop: 15,
    alignSelf: 'center',
  },
  linha2: {
    width: 280, 
    height: 2, 
    flex:1, 
    backgroundColor: 'white', 
    marginTop: 80,
    alignSelf: 'center',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    height: '100%',
    width: '100%',
  },
  buttonVoltar: {
    marginVertical: '15%',
    marginLeft: '20%',
    width: 45,
    height: 40,
    resizeMode: 'contain',
  },
  logoContainer: {
    flexDirection: 'row',
    height: '12%',
    width: '100%',
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
  logo: {
    marginVertical: '5%',
    marginRight: '5%',
    width: 150,
    height: 50,
    resizeMode: 'contain',
  },
  baseText: {
    fontFamily: 'Abel Regular',
    fontSize: 15,
    flexDirection: 'row',
    marginLeft: '5%',
    marginTop: '3.5%'
  },
  caixote: {
    backgroundColor: '#C4C4C4',
    flexDirection: 'column',
    marginHorizontal: '10%',
    marginTop: '8%',
    marginBottom: '1%',
    width: '80%',
    height: '27%',
  },
  textinho: {
    marginHorizontal: 15,
    marginTop: 20,
    fontFamily: 'Abel Regular',
    fontSize: 20,
    marginBottom: 20,
  },
  buttonProx: {
    alignItems: 'center',
    backgroundColor: '#57BC90',
    height: 50,
    fontSize: 100,
    marginHorizontal: 50,
    marginBottom: 20,
    marginTop: 5,
    elevation: 3,
  },
  textoLeitura: {
    color: 'white',
    marginTop: 11,
    fontFamily: 'Abel Regular',
    fontSize: 18
  },
  programadas: {
    marginLeft: 30,
    fontFamily: 'Abel Regular',
    fontSize: 15,
  },
  colaborador: {
    marginHorizontal: 20,
    marginTop: 20,
    fontFamily: 'Abel Regular',
    fontSize: 20,
    marginBottom: 20,
    textAlign: 'center',
  },
});
