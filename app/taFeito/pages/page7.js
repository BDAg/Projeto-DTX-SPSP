import React, { Component } from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  Image,
  TouchableOpacity,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

export default class Page7 extends Component {
  render() {
    return (
      <LinearGradient colors={['#F27281', '#345D7E']} style={styles.container}>
        <View>
          <View style={styles.logoContainer}>
            <TouchableOpacity style={{elevation: 3}} 
            onPress={()=> this.props.navigation.navigate('page6')}>
              <Image style={styles.buttonVoltar} source={require('../assets/images/voltar.png')} />
            </TouchableOpacity>
            <Image style={styles.logo} source={require('../assets/images/logo2.png')} />
          </View> 
          <Text style={styles.baseText}>OLÁ: MARIA DO CARMO</Text>
          <Text style={styles.baseText}>VOCÊ ESTÁ NO LOCAL: ÁREA EXTERNA</Text>
          <Text style={styles.baseText}>DO CLIENTE: SPSP</Text>
          <View style={styles.caixote}>
            <ScrollView>
              <Text style={styles.textinho}>
                VOCÊ DESEJA INICIAR A ROTINA SELECIONADA?
              </Text> 
              <Text style={styles.programadas}>
                LIMPEZA PROGRAMADA 09:00 - 09:20
              </Text> 
              <TouchableOpacity style={styles.buttonProx}
                onPress={()=>this.props.navigation.navigate('page8')}>
                <Text style={styles.textoLeitura}>INICIAR</Text>
              </TouchableOpacity>   
            </ScrollView>             
          </View>       
        </View>

      </LinearGradient>  
    );
  }
}

const styles = StyleSheet.create({  
  container: {
    flex: 1,
    flexDirection: 'column',
    height: '100%',
    width: '100%',
  },
  buttonVoltar: {
    marginVertical: '15%',
    marginLeft: '20%',
    width: 45,
    height: 40,
    resizeMode: 'contain',
  },
  logoContainer: {
    flexDirection: 'row',
    height: '12%',
    width: '100%',
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
  logo: {
    marginVertical: '5%',
    marginRight: '5%',
    width: 150,
    height: 50,
    resizeMode: 'contain',
  },
  baseText: {
    fontFamily: 'Abel Regular',
    fontSize: 15,
    flexDirection: 'row',
    marginLeft: '5%',
    marginTop: '3.5%'
  },
  caixote: {
    backgroundColor: '#C4C4C4',
    flexDirection: 'column',
    marginHorizontal: '10%',
    marginTop: '8%',
    marginBottom: '5%',
    width: '80%',
    height: '50%',
  },
  textinho: {
    marginHorizontal: 20,
    marginTop: 20,
    fontFamily: 'Abel Regular',
    fontSize: 20,
    marginBottom: 20,
  },
  buttonProx: {
    alignItems: 'center',
    backgroundColor: '#57BC90',
    height: 50,
    fontSize: 100,
    marginHorizontal: 50,
    marginTop: 15,
    marginBottom: 30,
    elevation: 3,
  },
  textoLeitura: {
    color: 'white',
    marginTop: 11,
    fontFamily: 'Abel Regular',
    fontSize: 18
  },
  programadas: {
    fontFamily: 'Abel Regular',
    fontSize: 15,
    textAlign: 'center',
    marginTop: 20,
  },
  colaborador: {
    marginHorizontal: 20,
    marginTop: 20,
    fontFamily: 'Abel Regular',
    fontSize: 20,
    marginBottom: 20,
    textAlign: 'center',
  },
});
