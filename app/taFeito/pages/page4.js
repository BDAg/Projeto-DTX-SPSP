import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Button,
  Image,
  TouchableOpacity,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

export default class Page4 extends Component {
  render() {
    return (
        <LinearGradient colors={['#F27281', '#345D7E']} style={styles.container}>
          <View>
            <View style={styles.logoContainer}>
              <TouchableOpacity style={{elevation: 3}} 
              onPress={()=> this.props.navigation.navigate('page3')}>
                <Image style={styles.buttonVoltar} source={require('../assets/images/voltar.png')} />
              </TouchableOpacity>
              <Image style={styles.logo} source={require('../assets/images/logo2.png')} />
            </View> 
            <TouchableOpacity style={styles.buttonProx}
            onPress={()=>this.props.navigation.navigate('page5')}>
              <Text style={styles.textoLeitura}>LEITURA DO CRACHÁ</Text>
            </TouchableOpacity>
            <Text style={styles.textinho}>
              IDENTIFIQUE-SE COMO COLABORADOR FAZENDO A LEITURA DO CÓDIGO DE BARRAS DO SEU CRACHÁ IDENTIFICADO COM A MARCA DO APP TA FEITO!
            </Text>           
          </View>
        </LinearGradient>           
    );
  }
}

const styles = StyleSheet.create({  
  container: {
    flex: 1,
    flexDirection: 'column',
    height: '100%',
    width: '100%',
  },
  buttonVoltar: {
    marginVertical: '15%',
    marginLeft: '20%',
    width: 45,
    height: 40,
    resizeMode: 'contain',
  },
  logoContainer: {
    flexDirection: 'row',
    height: '12%',
    width: '100%',
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
  logo: {
    marginVertical: '5%',
    marginRight: '5%',
    width: 150,
    height: 50,
    resizeMode: 'contain',
  },
  textinho: {
    marginHorizontal: 50,
    textAlign: 'center',
    marginVertical: '30%',
    fontFamily: 'Abel Regular',
    fontSize: 20,
  },
  buttonProx: {
    alignItems: 'center',
    backgroundColor: '#57BC90',
    height: '10%', 
    fontSize: 100,
    marginHorizontal: '10%',
    marginTop: '35%',
    elevation: 3,
  },
  textoLeitura: {
    color: 'white',
    paddingVertical: '5%',
    fontFamily: 'Abel Regular',
    fontSize: 25
  },
});

