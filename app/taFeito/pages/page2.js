import React, { Component } from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  Image,
  TextInput,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import { RNCamera, FaceDetector } from 'react-native-camera';

const { width } = Dimensions.get('screen');

export default class Page2 extends Component {
  barcodeRecognized = ({ barcodes }) => {
    this.props.navigation.replace('page3');
    // barcodes.forEach(barcode => console.warn(barcode.data))
  };
  render() { 
    const leftTop = {
      borderLeftWidth: 3,
      borderTopWidth: 3,
      borderColor: 'white'
    };
    const leftBottom = {
      borderLeftWidth: 3,
      borderBottomWidth: 3,
      borderColor: 'white'
    };
    const rightTop = {
      borderRightWidth: 3,
      borderTopWidth: 3,
      borderColor: 'white'
    };
    const rightBottom = {
      borderRightWidth: 3,
      borderBottomWidth: 3,
      borderColor: 'white'
    };
    return (
      <LinearGradient colors={['#F27281', '#345D7E']} style={styles.container}>
        <ScrollView>
          <View>
            <View style={styles.logoContainer}>
              <TouchableOpacity style={{elevation: 3}} 
              onPress={()=> this.props.navigation.navigate('page1')}>
                <Image style={styles.buttonVoltar} source={require('../assets/images/voltar.png')} />
              </TouchableOpacity>
              <Image style={styles.logo} source={require('../assets/images/logo2.png')} />
            </View> 
            <View>
              <RNCamera style={styles.cam}
              onGoogleVisionBarcodesDetected={this.barcodeRecognized}
              googleVisionBarcodeType={RNCamera.Constants.GoogleVisionBarcodeDetection.BarcodeType.QR_CODE}
              autoFocus={RNCamera.Constants.AutoFocus.on}
              />             
            </View>
            <View style={styles.marker}>
              <View style={{ flex:1, flexDirection: 'row' }}>
                <View style={{ flex:1, ...leftTop }}></View>
                <View style={{ flex:1 }}></View>
                <View style={{ flex:1, ...rightTop }}></View>
              </View>
              <View style={{ flex:1 }}></View>
              <View style={{ flex:1, flexDirection: 'row' }}>
                <View style={{ flex:1, ...leftBottom }}></View>
                <View style={{ flex:1 }}></View>
                <View style={{ flex:1, ...rightBottom }}></View>
              </View>
            </View>
            <Text style={styles.textinho}>
              POSICIONE A CAMERA DO APP EM FRENTE AO QR CODE DO LOCAL
            </Text>           
          </View>
        </ScrollView>
      </LinearGradient>  
    )
  }
};

const styles = StyleSheet.create({  
  container: {
    flex: 1,
    flexDirection: 'column',
    height: '100%',
    width: '100%',
  },
  buttonVoltar: {
    marginVertical: '15%',
    marginLeft: '20%',
    width: 45,
    height: 40,
    resizeMode: 'contain',
  },
  logoContainer: {
    flexDirection: 'row',
    height: '12%',
    width: '100%',
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
  logo: {
    marginVertical: '5%',
    marginRight: '5%',
    width: 150,
    height: 50,
    resizeMode: 'contain',
  },
  marker: {
    width: width / 2,
    height: width / 2,
    marginVertical: 120,
    alignSelf: 'center',
  },
  cam: {
    position: 'absolute',
    width: 290,
    height: 140,
    marginTop: '35%',
    alignSelf: 'center'
  },
  textinho: {
    marginHorizontal: '15%',
    paddingTop: '2%',
    textAlign: 'center',
    fontFamily: 'Abel Regular',
    fontSize: 20,
  },
});
