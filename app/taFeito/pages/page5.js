import React, { Component } from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  Image,
  TextInput,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import { RNCamera, FaceDetector } from 'react-native-camera';

const { width } = Dimensions.get('screen');

export default class Page5 extends Component {
  barcodeRecognized = ({ barcodes }) => {
    this.props.navigation.navigate('page6');
    barcodes.forEach(barcode => console.warn(barcode.data))
  };
  render() {
    return (
      <LinearGradient colors={['#F27281', '#345D7E']} style={styles.container}>
        <ScrollView>
          <View>
            <View style={styles.logoContainer}>
              <TouchableOpacity style={{elevation: 3}} 
              onPress={()=> this.props.navigation.navigate('page4')}>
                <Image style={styles.buttonVoltar} source={require('../assets/images/voltar.png')} />
              </TouchableOpacity>
              <Image style={styles.logo} source={require('../assets/images/logo2.png')} />
            </View> 
            <View>
              <RNCamera style={styles.cam}
              onGoogleVisionBarcodesDetected={this.barcodeRecognized}
              googleVisionBarcodeType={RNCamera.Constants.GoogleVisionBarcodeDetection.BarcodeType.CODABAR}
              autoFocus={RNCamera.Constants.AutoFocus.on}
              />             
            </View>
            <View style={styles.marker}/>
            <Text style={styles.textinho}>
              POSICIONE A CAMERA DO APP EM FRENTE AO CÓDIGO DE BARRAS DO SEU CRACHÁ
            </Text>           
          </View>
        </ScrollView>
      </LinearGradient>  
    )
  }
};

const styles = StyleSheet.create({  
  container: {
    flex: 1,
    flexDirection: 'column',
    height: '100%',
    width: '100%',
  },
  buttonVoltar: {
    marginVertical: '15%',
    marginLeft: '20%',
    width: 45,
    height: 40,
    resizeMode: 'contain',
  },
  logoContainer: {
    flexDirection: 'row',
    height: '12%',
    width: '100%',
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
  logo: {
    marginVertical: '5%',
    marginRight: '5%',
    width: 150,
    height: 50,
    resizeMode: 'contain',
  },
  marker: {
    width: '60%',
    height: 3,
    marginTop: '60%',
    alignSelf: 'center',
    backgroundColor: 'red',
  },
  cam: {
    position: 'absolute',
    width: 290,
    height: 140,
    marginTop: '35%',
    alignSelf: 'center'
  },
  textinho: {
    marginHorizontal: '15%',
    paddingTop: '50%',
    textAlign: 'center',
    fontFamily: 'Abel Regular',
    fontSize: 20,
  },
});
