import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  Image,
  TouchableOpacity,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

export default class Page3 extends Component {
  render() {
    return (
      <LinearGradient colors={['#F27281', '#345D7E']} style={styles.container}>
        <View>
          <View style={styles.logoContainer}>
            <TouchableOpacity style={{elevation: 3}} 
            onPress={()=> this.props.navigation.pop()}>
              <Image style={styles.buttonVoltar} source={require('../assets/images/voltar.png')} />
            </TouchableOpacity>
            <Image style={styles.logo} source={require('../assets/images/logo2.png')} />
          </View> 
          <Text style={styles.baseText}>OLÁ</Text>
          <Text style={styles.baseText}>VOCÊ ESTÁ NO LOCAL: ÁREA EXTERNA</Text>
          <Text style={styles.baseText}>DO CLIENTE: SPSP</Text>
          <View style={styles.caixote}>
            <ScrollView>
              <Text style={styles.textinho}>
                ÚLTIMAS ROTINAS REALIZADAS
              </Text> 
              <Text style={styles.reticencias}>
                ...
              </Text> 
              <Text style={styles.reticencias}>
                ...
              </Text> 
              <View style={styles.linha1}/> 
              <Text style={styles.textinho}>
                PRÓXIMAS ROTINAS PROGRAMADAS
              </Text> 
              <Text style={styles.programadas}>
                PRODUÇÃO PROGRAMADA 07:30 - 08:00
              </Text> 
              <Text style={styles.programadas}>
                LIMPEZA PROGRAMADA 09:00 - 09:20
              </Text> 
              <Text style={styles.programadas}>
                PRODUÇÃO PROGRAMADA 14:15 - 14:30
              </Text> 
              <Text style={styles.programadas}>
                LIMPEZA PROGRAMADA 14:30 - 15:00
              </Text> 
              <View style={styles.linha1}/> 
              <Text style={styles.colaborador}>
                ÁREA DO COLABORADOR
              </Text>   
              <TouchableOpacity style={styles.buttonProx}
                onPress={()=>this.props.navigation.navigate('page4')}>
                <Text style={styles.textoLeitura}>VERIFICAR ROTINA</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>       
        </View>
      </LinearGradient>  
    );
  }
}

const styles = StyleSheet.create({  
  linha1: {
    width: 280, 
    height: 2, 
    flex:1, 
    backgroundColor: 'white', 
    marginTop: 15,
    alignSelf: 'center',
  },
  linha2: {
    width: 280, 
    height: 2, 
    flex:1, 
    backgroundColor: 'white', 
    marginTop: 80,
    alignSelf: 'center',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    height: '100%',
    width: '100%',
  },
  buttonVoltar: {
    marginVertical: '15%',
    marginLeft: '20%',
    width: 45,
    height: 40,
    resizeMode: 'contain',
  },
  logoContainer: {
    flexDirection: 'row',
    height: '12%',
    width: '100%',
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
  logo: {
    marginVertical: '5%',
    marginRight: '5%',
    width: 150,
    height: 50,
    resizeMode: 'contain',
  },
  baseText: {
    fontFamily: 'Abel Regular',
    fontSize: 15,
    flexDirection: 'row',
    marginLeft: '5%',
    marginTop: '3.5%'
  },
  caixote: {
    backgroundColor: '#C4C4C4',
    flexDirection: 'column',
    marginHorizontal: '10%',
    marginTop: '8%',
    marginBottom: '5%',
    width: '80%',
    height: '60%',
  },
  reticencias: {
    marginHorizontal: 30, 
    marginTop: -8,
    fontFamily: 'Abel Regular',
    fontSize: 20,
  },
  textinho: {
    marginHorizontal: 20,
    marginTop: 20,
    fontFamily: 'Abel Regular',
    fontSize: 20,
    marginBottom: 8,
  },
  buttonProx: {
    alignItems: 'center',
    backgroundColor: '#57BC90',
    height: 50,
    fontSize: 100,
    marginHorizontal: 50,
    marginBottom: 40,
    elevation: 3,
  },
  textoLeitura: {
    color: 'white',
    marginTop: 11,
    fontFamily: 'Abel Regular',
    fontSize: 18
  },
  programadas: {
    marginLeft: 30,
    fontFamily: 'Abel Regular',
    fontSize: 15,
  },
  colaborador: {
    marginHorizontal: 20,
    marginTop: 20,
    fontFamily: 'Abel Regular',
    fontSize: 20,
    marginBottom: 20,
    textAlign: 'center',
  },
});
