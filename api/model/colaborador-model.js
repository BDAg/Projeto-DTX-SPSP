const mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

const colaboradorModel = new mongoose.Schema({
    name: {
        type: String,
        required: true
    }
}, {collection:'Colaboradores'} )

module.exports = mongoose.model('Colaborador', colaboradorModel);
