const mongoose = require('mongoose');
const schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

const localModel = new schema({
    cliente_id: {
        type: ObjectId,
        ref: 'Cliente'
    },
    thumbnail:{
        type: String,
    },
    companhia: {
        type: String,
        required: true
    },
    nomeLocal: {
        type: String,
        requred: true
    },
    rotinasLista: {
        type: Array
    }
    /*
    {
        diaSemana: {
            type: String,
            required: true
        },
        tipoRotina: {
            type: String,
            required: true
        },
        listaAtividades: {
            type: Array,
            required: true,
            
        },
        listaEquipamentos: {
            type: Array,
            required: true,
            
        },
        listaFerramentas: {
            type: Array,
            required: true,
            
        },
        listaProdutos: {
            type: Array,
            required: true,
            
        },
        inicio: {
            type: String,
            required: true
        },
        termino: {
            type: String,
            required: true
        }
    }
    */
}, { collection: 'locais' });

module.exports = mongoose.model('Local', localModel);