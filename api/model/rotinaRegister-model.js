const mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

const registra_RotinasModel = new mongoose.Schema({
    date: String,
    approved: Boolean,
    colaborador:{
        type: ObjectId,
        ref: 'Colaborador'
    },
    local:{
        type: ObjectId,
        ref: 'Local'
    } 
}, {collection:'rotinasRegistradas'});

module.exports = mongoose.model('Rotinas', registra_RotinasModel);
