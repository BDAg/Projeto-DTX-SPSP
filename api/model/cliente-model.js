const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');


const ClienteSchema = new mongoose.Schema({
    name: {
        type: String,
        require: true,
    },
    email: {
        type: String,
        unique: true,
        required: true,
        lowercase: true,
    },
    password: {
        type: String,
        required: true,
        select: false,
    },
    createAt: {
        type: Date,
        default: Date.now,
    },
    companhia: { 
        type: String, 
        required: true 
    },
    cnpj: { 
        type: String, 
        required: true 
    },
    cidade: {
        type: String, 
        required: true 
    },
    pais: { 
        type: String, 
        required: true 
    },
    endereco: { 
        type: String, 
        required: true 
    },
    categoria: { 
        type: String, 
        required: true 
    }
    
},{ collection: 'Clientes'});

ClienteSchema.pre('save', async function (next) {
    const hash = await bcrypt.hash(this.password, 10);
    this.password = hash;

    next();
});

const Cliente = mongoose.model('Cliente', ClienteSchema)
module.exports = Cliente; 