const mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

const readQrcode_Model = new mongoose.Schema({
    createAt: {
        type: Date,
        default: Date.now,
    },
    approved: Boolean,
    cliente_id: {
        type: ObjectId,
        ref: 'Cliente'
    },
    local:{
        type: ObjectId,
        ref: 'Local'
    },
    local_token:{
        type: String,
        required: true,
    } 
        
    
}, {collection:'readQrcode'});

module.exports = mongoose.model('readQrcode', readQrcode_Model);
