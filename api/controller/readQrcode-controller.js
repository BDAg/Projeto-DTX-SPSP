const ReadQrcode = require('../model/readQrcode-model');



module.exports ={
    async lerQrcode(req, res) {
        const { cliente_id, local_id, local_token } = req.body;
    
        

        const readQrcode = await ReadQrcode.create({
            cliente: cliente_id,
            local: local_id,
            local_token,
        });
        
        await readQrcode.populate('local').execPopulate();
        await readQrcode.populate('cliente').execPopulate();
        
        

        return await res.json(readQrcode);

    }
}