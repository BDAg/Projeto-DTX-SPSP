const RotinaRegister = require('../model/rotinaRegister-model')

module.exports ={
    async store(req, res) {
        const { colaborador_id } = req.headers;
        const { local_id } = req.headers;
        const { date } = req.body;


        const rotinaRegister = await RotinaRegister.create({
            colaborador: colaborador_id,
            local: local_id,
            date,
        });
        
        await rotinaRegister.populate('local').execPopulate();
        await rotinaRegister.populate('colaborador').execPopulate();
        
        console.log('Colaborador: '+`${colaborador_id}`+ ' registrou a rotina no local: ',+`${local_id}`);

        return await res.json(rotinaRegister);
    }
}