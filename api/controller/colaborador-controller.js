const Colaborador = require('../model/colaborador-model');
var JsBarcode = require('jsbarcode');
var barcode = require('../JsBarcode.all.min.js')



exports.register = async (req, res) => {
    const { name } = req.body;

    try{
        if (await Colaborador.findOne({ name }))
            return res.status(400).send({error: 'Colaborador already exists'});
        
    
        const colaborador = await Colaborador.create(req.body)
        

        var codigo = new JsBarcode(barcode, "1554466",{
            width:5,
            height:200,
            fontSize:50,
            displayValue: true
        });

        console.log(codigo);

        console.log('\nColaborador '+name+' foi registrado');

        return res.send({
            colaborador
        });
    } catch (err) {
        return res.status(400).send({ error: 'Registration failed'});
    }

    
    
};


// exports.authenticate = async (req, res) => {
//     const { name, companhia, nomeLocal, _idLocal } = req.body;

//     const colaborador = await Colaborador.findOne({ name, companhia, nomeLocal, _idLocal });
//     console.log('\n'+name+' fez o chekin em: '+companhia + '\n'+ _idLocal +'\n'+ nomeLocal);

//     if (!colaborador)
//         return res.status(400).send({ error:'Colaborador not found or invalid name' });
    
//     res.send({
//         colaborador
        
//     });
// };

exports.find_all = function(req, res) {
    Colaborador.find().limit().exec((err, value)=>{
        res.json(value);

        console.log('Todos os colaboradores foram listados');
    })
};
