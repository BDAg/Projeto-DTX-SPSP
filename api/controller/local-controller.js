const jwt = require('jsonwebtoken');
var QRCode = require('qrcode');


const Local = require('../model/local-model');

const authConfig = require('../bin/configuration/auth');


exports.store = async (req, res) => {
    const { filename } = req.file;
    const { cliente_id } = req.headers;
    const { companhia, nomeLocal, rotinasLista } = req.body;

    //QRCode com o nome do ambiente
    function nomeAmbiente(req,res){
        QRCode.toFile('qr.png',String(req), function (err, url){
            console.log(url);
        })
    }
    //Encriptando QRCode
    // const { nomeLocal} = req.body;

    var token = jwt.sign({nomeLocal}, 'joker')

    let tokenDecoded = jwt.verify(token, 'joker');

    console.log(tokenDecoded);

    //Printando QRCode encriptado
    nomeAmbiente(token);

    //Decodificando o token
    var decoded = jwt.decode(token, {complete: true});
    console.log(decoded.header);
    console.log(decoded.payload);
    

        if (await Local.findOne({cliente_id, filename, companhia, nomeLocal, rotinasLista}))
            return res.status(400).send({error:'Local already exists'});
        
        const local = await Local.create({
            'thumbnail': filename,
            'cliente_id': cliente_id,
            'companhia': companhia, 
            'nomeLocal': nomeLocal, 
            'rotinasLista': rotinasLista,
            'token': generateToken({ id: cliente_id.id })
        });

        console.log('\n'+req.body.nomeLocal+' Foi Registrado para o cliente: '+ req.body.companhia +' \n do ID:'+ req.params.cliente_id);
        
        await local.populate('Cliente').execPopulate();
        
        function generateToken(params = {}) {
            return jwt.sign(params, authConfig.secret, {
                expiresIn: 86400,
            });
        }

        return res.send({
            local,
            token: generateToken({ id: local.id })
        });
        
    
   

};

exports.find_all = function(req, res) {
    
    Local.find().limit().exec((err, value)=>{
        res.json(value);
    
        console.log('Todos os locais foram listados');
    });
};

exports.authenticate = function (req,res) {
    
    let { token } = req.body;

    if (token == undefined) {
        res.status(406).json({
            "error": "Invalid"
        });
    } else {

        let tokenDecoded = jwt.verify(token, 'joker');

        console.log(tokenDecoded);

        if ('id' in tokenDecoded) {
            res.status(200).json({ 'pass': true });
        } else {
            res.status(406).json({error: 'rejeitado'});
        }

        res.json();

    }



};