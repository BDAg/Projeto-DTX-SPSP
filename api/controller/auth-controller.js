const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');




const authConfig = require('../bin/configuration/auth');

const Cliente = require('../model/cliente-model');

// const router = express.Router();

function generateToken(params = {}) {
    return jwt.sign(params, authConfig.secret, {
        expiresIn: 86400,
    });
}


exports.register = async (req,res) => {

    const {email} = req.body;
 

    try {
        if (await Cliente.findOne({email}))
            return res.status(400).send({error:'Email of the user already exists'});

        const cliente = await Cliente.create(req.body);

        cliente.password = undefined;

        console.log('\n'+email+' Foi Registrado');

        return res.send({
            cliente,
            token: generateToken({ id: cliente.id })
        });

    } catch (err) {
        return res.status(400).send({ error: 'Registration failed'});
    }


};


exports.authenticate = async (req, res) => {
    const { email, password } = req.body;

    const cliente = await Cliente.findOne({ email }).select('+password');

   

    if (!cliente)
        return res.status(400).send({ error : 'User not found'});
//analisa se a senha é igual a senha cadastrada
    if (!await bcrypt.compare(password, cliente.password))
        return res.status(400).send({ error: 'Invalid password'});

    cliente.password = undefined;

    
    console.log('\n'+email+' Foi Autenticado');
    res.send({
        cliente,
        token: generateToken({ id: cliente.id }) 
    });
};


exports.find_all = function(req, res) {
    Cliente.find().limit().exec((err, value)=>{
        res.json(value);
    
        console.log('Todos os usuários usuários foram listados');
    });
};