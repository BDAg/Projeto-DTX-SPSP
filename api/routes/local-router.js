const express = require('express');


const multer = require('multer');
const uploadConfig = require('../bin/configuration/upload.js');

const router = express.Router();
const upload = multer(uploadConfig);

const localcontroller = require ('../controller/local-controller');

router.get('/list', localcontroller.find_all);
router.post('/register',upload.single('thumbnail'), localcontroller.store);
router.post('/auth', localcontroller.authenticate);


module.exports = router;