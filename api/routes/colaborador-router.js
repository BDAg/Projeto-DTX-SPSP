const express = require('express');
const router = express.Router();

const colaboradorcontroller = require ('../controller/colaborador-controller');

router.get('/list', colaboradorcontroller.find_all);
router.post('/register', colaboradorcontroller.register);
// router.post('/authenticate', colaboradorcontroller.authenticate);

module.exports = router;