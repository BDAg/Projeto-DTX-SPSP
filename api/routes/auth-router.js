const express = require('express');
const router = express.Router();

const authcontroller = require ('../controller/auth-controller');

router.get('/list', authcontroller.find_all);
router.post('/register', authcontroller.register);
router.post('/authenticate', authcontroller.authenticate);

module.exports = router;