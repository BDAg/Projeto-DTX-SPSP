const express = require('express');
const router = express.Router();

const readQrcode = require ('../controller/readQrcode-controller');

router.post('/qrcode', readQrcode.lerQrcode);

module.exports = router;