
const variables = {
    Api: {
        port: process.env.port || 8080
    },
    Database: {
        connection: process.env.connection || 'mongodb+srv://dtxspsp:dtxspsp6@appdb-lioe5.mongodb.net/test?retryWrites=true&w=majority'
    }
}

module.exports = variables;