const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const variables = require('../bin/configuration/variables');
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

//Router:

// const localRoute = require('../routes/local-router');
const authRoute = require('../routes/auth-router');
const colaboradorRoute = require('../routes/colaborador-router');
const localRoute = require('../routes/local-router');
const rotinaRegisterRoute = require('../routes/rotinaRegister-router');
const readQrcodeRoute = require('../routes/readQrcode-router');


const app = express();


//COnfiguração parser do JSON
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));

mongoose.connect(variables.Database.connection, { useNewUrlParser: true });


app.use('/auth/cliente', authRoute);
app.use('/colaboradores', colaboradorRoute);
app.use('/local', localRoute);
app.use('/:local_id', rotinaRegisterRoute);

app.use('/readqrcode', readQrcodeRoute);




module.exports = app;
